import avatar from '../../assets/low_no_avatar.jpg';
import {Avatar, Box, Button, Flex, Link, Text} from '@chakra-ui/react';

const Header = () => {
    return (
        <Box borderBottom='1px' borderBottomColor='#10161A26' py='15px'>
            <Flex justify='space-around'>
                <Text fontSize='4xl' color='#394b59' fontWeight='bold'>Logo</Text>
                <Flex alignItems='center'>
                    <Avatar mr='10px' size='sm' name='avatar' src={avatar} />
                    <Text mr='20px' fontWeight='bold'>gavrilovavv</Text>
                    <Link href='/'>
                        <Button borderColor='#18202633' borderRadius='3px' bg='white'>
                            Выход
                        </Button>
                    </Link>
                </Flex>
            </Flex>
        </Box>
    );
};

export default Header;
