import React from 'react';
import {Box, Flex, FormLabel, Input, Text} from "@chakra-ui/react";
import CardItem from "../CardItem/CardItem";

const Body = (props: {
    selected: boolean,
    addFileHandler: React.ChangeEventHandler<HTMLInputElement>,
    drag: boolean,
    fileInfo: { name: string; size: number; url: string }[],
}) => {
    return (
        <Flex flexGrow='1' flexDirection='column' alignItems='center' bg='#f5f8fa' pt='80px'>
            {
                props.selected
                    ? <FormLabel position='relative' border='2px dashed #48aff0' py='60px' px='280px' bg='white' mb='20px'>
                        <svg viewBox="0 0 24 24" width="1em" height="1em" className="iconUpload">
                            <path fill="currentColor"
                                  d="M14 13v4h-4v-4H7l5-5 5 5m2.35-2.97A7.49 7.49 0 0012 4C9.11 4 6.6 5.64 5.35 8.03A6.004 6.004 0 000 14a6 6 0 006 6h13a5 5 0 005-5c0-2.64-2.05-4.78-4.65-4.97z"></path>
                        </svg>
                        <Input type='file' id='file' display='none' onChange={props.addFileHandler} multiple/>
                        {
                            props.drag
                                ? <Box color='#4296ca' textTransform='uppercase' fontWeight='bold' textAlign='center'>
                                    Перетащите файлы в окно браузера <br/>
                                    чтобы начать загрузку
                                    <Box position='fixed' bg='#82929f70' w='100%' h='100%' top='0' left='0'/>
                                </Box>
                                : <Box border='1px solid #0b3249' bg='#4296ca' color='white' py='7px' px='15px'
                                       _hover={{bg: '#145b85'}}>
                                    Загрузить картинки
                                </Box>
                        }
                    </FormLabel>
                    : <FormLabel position='relative' border='2px dashed #48aff0' py='130px' px='140px' bg='white' mb='20px'>
                        <svg viewBox="0 0 24 24" width="1em" height="1em" className="iconUpload">
                            <path fill="currentColor"
                                  d="M14 13v4h-4v-4H7l5-5 5 5m2.35-2.97A7.49 7.49 0 0012 4C9.11 4 6.6 5.64 5.35 8.03A6.004 6.004 0 000 14a6 6 0 006 6h13a5 5 0 005-5c0-2.64-2.05-4.78-4.65-4.97z"></path>
                        </svg>
                        <Flex flexDirection='column' alignItems='center'>
                            <Input type='file' id='file' display='none' onChange={props.addFileHandler} multiple/>
                            {
                                props.drag
                                    ? <Box color='#4296ca' textTransform='uppercase' fontWeight='bold' textAlign='center'>
                                        Перетащите файлы в окно браузера <br/>
                                        чтобы начать загрузку
                                        <Box position='fixed' bg='#82929f70' w='100%' h='100%' top='0' left='0'/>
                                    </Box>
                                    : <>
                                        <Text align='center' mb='20px' fontWeight='bold'>
                                            Перетащите изображения в любое место на экране<br/>
                                            или используйте кнопку
                                        </Text>
                                        <Box border='1px solid #0b3249' bg='#4296ca' color='white' py='7px' px='15px'
                                             _hover={{bg: '#145b85'}}>
                                            Загрузить картинки
                                        </Box>
                                    </>
                            }
                        </Flex>
                    </FormLabel>
            }
            <Text align='center' color='#394b59' mb='30px'>
                Можно загружать: png, gif, jpeg; размеры до 8192x8192px; максимальный размер файла — 20MB
            </Text>
            {
                props.fileInfo.map((el: { name: string; size: number; url: string }, index: number) => {
                    return <CardItem key={index} url={el.url} name={el.name} size={el.size}/>
                })
            }
        </Flex>
    );
};

export default Body;
