import React, {useState} from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faImage, faLink, faCode, faMarker} from '@fortawesome/free-solid-svg-icons';
import {Box, Flex, Image, Text, Link} from '@chakra-ui/react';
import icon from '../../assets/magnifying-glass-plus-solid.svg';

const CardItem = (props: {
    name: string,
    size: number,
    url: string,
}) => {
    const [size, setSize] = useState<{ width: number, height: number }[]>([]);
    const getSize = (e: React.SyntheticEvent<HTMLImageElement>) => {
        setSize([{
        width: e.currentTarget.naturalWidth,
        height: e.currentTarget.naturalHeight,
        }]);
    }

    return (
        <Box border='1px solid #10161A26' py='10px' px='15px' mb='30px' bg='white'>
            <Flex justifyContent='space-between' borderBottom='1px solid #10161A26' mb='10px' pb='15px'>
                <Flex>
                    <FontAwesomeIcon icon={faImage}/>
                    <Text ml='10px'>{props.name}</Text>
                </Flex>
                <Box>{props.size} Bytes</Box>
                {size.map((el, index) => {
                    return <Box key={index}>{el.width}x{el.height}</Box>
                })}
            </Flex>
            <Flex position='relative'>
                <Image onLoad={(e) => getSize(e)} src={props.url} alt='img' objectFit='cover' mr='20px'
                       boxSize='160px'/>
                <Link position='absolute' display='flex' justifyContent='center' alignItems='center' boxSize='160px'
                      _hover={{bg: '#000000B2'}} href={props.url} target='_blank' rel='noreferrer'>
                    <Image display='none' boxSize='60px' zIndex='100' position='relative' _hover={{display: 'block'}}
                           src={icon} alt='icon'/>
                </Link>
                <Box>
                    <Link display='flex' border='1px solid #10161A26' p='10px' mb='10px'>
                        <FontAwesomeIcon icon={faLink}/>
                        <Box ml='10px'>{props.url}</Box>
                    </Link>
                    <Link display='flex' border='1px solid #10161A26' p='10px' mb='10px'>
                        <FontAwesomeIcon icon={faCode}/>
                        <Box ml='10px'>{props.url}</Box>
                    </Link>
                    <Link display='flex' border='1px solid #10161A26' p='10px' mb='10px'>
                        <FontAwesomeIcon icon={faMarker}/>
                        <Box ml='10px'>{props.url}</Box>
                    </Link>
                </Box>
            </Flex>
        </Box>
    );
};

export default CardItem;
