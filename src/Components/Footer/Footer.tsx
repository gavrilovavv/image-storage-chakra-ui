import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faLanguage} from '@fortawesome/free-solid-svg-icons';
import {Box, Flex, Link, ListItem, Select, UnorderedList} from "@chakra-ui/react";

const Footer = () => {
    return (
        <Box borderTop='1px' borderTopColor='#10161A26' py='10px'>
            <Flex alignItems='center' justifyContent='center'>
                <Flex alignItems='center' px='10px' _hover={{bg: '#A7B6C24C', borderRadius: '5px'}}>
                    <FontAwesomeIcon icon={faLanguage}/>
                    <Select border='none' color='#31383d'>
                        <option>Русский</option>
                        <option>English</option>
                    </Select>
                </Flex>
                <UnorderedList display='flex' styleType='none' fontSize='.8rem' color='#106ba3'>
                    <ListItem mr='10px'>
                        <Link href='/'>Условия использования</Link>
                    </ListItem>
                    <ListItem mr='10px'>
                        <Link href='/'>Лицензионное соглашение</Link>
                    </ListItem>
                    <ListItem mr='10px'>
                        <Link href='/'>Политика конфиденциальности</Link>
                    </ListItem>
                </UnorderedList>
                <Box>© 2023 <Link href='/' color='#106ba3'>Gaijin Network Ltd</Link></Box>
            </Flex>
        </Box>
    );
};

export default Footer;
