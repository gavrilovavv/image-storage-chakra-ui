import './App.css';
import Header from './Components/Header/Header';
import {ChakraProvider, Flex} from '@chakra-ui/react'
import Footer from './Components/Footer/Footer';
import Body from './Components/Body/Body';
import {DragEvent, useState} from 'react';

const png = 'image/png';
const jpeg = 'image/jpeg';
const gif = 'image/gif';

function App() {
    const [drag, setDrag] = useState<boolean>(false);
    const [fileInfo, setFileInfo] = useState<{ name: string, size: number, url: string }[]>([]);
    const [selected, setSelected] = useState<boolean>(false);

    const dragStartHandler = (e: DragEvent<HTMLDivElement>) => {
        e.preventDefault();
        setDrag(true);
    }

    const dragLeaveHandler = (e: DragEvent<HTMLDivElement>) => {
        e.preventDefault();
        setDrag(false);
    }

    const dropHandler = (e: DragEvent<HTMLDivElement>) => {
        e.preventDefault();

        for (let i = 0; i < e.dataTransfer.files.length; i++) {
            if (e.dataTransfer.files[i].type === png ||
                e.dataTransfer.files[i].type === jpeg ||
                e.dataTransfer.files[i].type === gif) {
                setFileInfo(prevInfo => [...prevInfo, {
                    name: e.dataTransfer.files[i].name,
                    size: e.dataTransfer.files[i].size,
                    url: window.URL.createObjectURL(e.dataTransfer.files[i]),
                }]);
            } else {
                setDrag(false);
                return;
            }
        }
        setDrag(false);
        setSelected(true);
    }

    const addFileHandler = () => {
        let fileInput = document.querySelector<HTMLInputElement>('#file');
        if (!fileInput || !fileInput.files) {
            return;
        }

        let files = fileInput.files;

        for (let i = 0; i < files.length; i++) {
            if (files[i].type === png ||
                files[i].type === jpeg ||
                files[i].type === gif) {
                setFileInfo(prevInfo => [...prevInfo, {
                    name: files[i].name,
                    size: files[i].size,
                    url: window.URL.createObjectURL(files[i]),
                }]);
                setSelected(true);
            } else {
                return;
            }
        }
    }

    return (
        <ChakraProvider>
            <Flex flexDirection='column' height='100vh'
                  onDragEnter={e => dragStartHandler(e)}
                  onDragLeave={e => dragLeaveHandler(e)}
                  onDragOver={e => dragStartHandler(e)}
                  onDrop={e => dropHandler(e)}>
                <Header />
                <Body fileInfo={fileInfo} selected={selected} drag={drag} addFileHandler={addFileHandler} />
                <Footer />
            </Flex>
        </ChakraProvider>
    );
}

export default App;
